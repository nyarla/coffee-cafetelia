"use stirct"

test = require('chai').expect

kbucket  = require('../src/bucket')
kcontact = require('../src/contact')

now      = Date.now()

describe 'Bucket:', ->

    it 'basic test', (done) ->
        bucket = kbucket.createBucket();

        test(bucket).to.be.an.instanceof kbucket.Bucket;

        for idx in [ 1..160 ]
            contact = kcontact.createContact '127.0.0.1', (50000 + idx)
            contact.lastSeen = now + idx

            # insert
            bucket.insert contact;
            test(bucket.size()).to.equal idx

            # get
            item   = bucket.get(0)

            test(item).to.be.an.instanceof kcontact.Contact

            # exists
            test(bucket.exists item ).to.ok;

        for idx in [ 159 .. 0 ]
            # remove
            bucket.remove bucket.get(idx)

            test(bucket.size()).to.equal idx
            

        test(bucket.size()).to.equal 0

        done()
