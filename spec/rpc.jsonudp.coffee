"use strict"

test  = require('chai').expect
RPC   = require('../src/rpc/jsonudp')
msg   = require('../src/message')
utils = require('../src/utils')

describe 'RPC.JSONUDP: ', ->
    Servers = []

    it 'create Server ok', ->
        for port in [ 1 .. 11 ]
            server  = null
            portNum = 55000 + port

            server  = RPC '127.0.0.1', portNum, ( err, message ) ->
                if message.rpcID && message.method == 'PING'
                    to =
                        address: message.from.address
                        port:    message.from.port

                    from =
                        address: '127.0.0.1'
                        port:    portNum
                        nodeID:  utils.id("127.0.0.1:#{portNum}")

                    resp = msg.createMessage 'PONG', from,
                        replyTo: message.rpcID

                    server.send to, resp

            test(server).to.ok

            Servers.push server

    it 'start Server ok', ->
        for server in Servers
            server.start()

        test(true).to.ok

    it 'send Message ok', (done) ->
        fromIdx = Math.floor( ( Math.random() * 10 ) + 1)
        toIdx   = Math.floor( ( Math.random() * 10 ) + 1)

        fromServer = Servers[fromIdx - 1]
        fromPort   = 55000 + fromIdx
        Message    = msg.createMessage('PING',
            address: '127.0.0.1'
            port:    fromPort
            nodeID:  utils.id("127.0.0.1:#{fromPort}")
        )

        toPort     = 55000 + toIdx

        console.log "#{fromPort} -> #{toPort}"

        fromServer.send
            address: '127.0.0.1'
            port:    toPort
            , Message, ( err, resp ) ->
                test(err).to.null
                test(resp).to.ok
                test(resp.method).to.equal 'PONG'

                done()

        
        