"use strict"

test = require('chai').expect

knode = require('../src/node')
rpc   = require('../src/rpc/emulator')
store = require('../src/storage/memory')

describe 'Node: ', ->

    before ->
        rpc.registerSuccessRule ( sendTo, message ) ->
            return true
    
    it 'basic test ok', (done) ->
        nodeA = knode( '127.0.0.1', 5000, rpc, store )
        nodeB = knode( '127.0.0.1', 5100, rpc, store )

        nodeA.start()
        nodeB.start()

        nodeA.connect '127.0.0.1', 5100, ( connErr ) ->
            test(connErr).to.not.ok

            nodeA.set 'foo', 'bar', ( setErr ) ->
                test(setErr).to.not.ok

                nodeA.get 'foo', ( getErr, val ) ->
                    test(getErr).to.not.ok

                    test(val).to.equal 'bar'

                    done()