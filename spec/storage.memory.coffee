"use strict"

test    = require('chai').expect
storage = require('../src/storage/memory')

describe 'Storage.Memory: ', ->

    it 'basic test ok', (done) ->
        store = storage()

        store.set 'foo', 'bar', ( err ) ->
            store.get 'foo', ( err, val ) ->
                test(val).to.equal 'bar'
                done()
