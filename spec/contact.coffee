"use strict"

test    = require('chai').expect
contact = require('../src/contact')

describe 'Contact:', ->

    it 'create Instance', (done) ->
        cont = contact.createContact '127.0.0.1', 50001

        test(cont).to.be.an.instanceof contact.Contact

        test(cont.address).to.equal '127.0.0.1'
        test(cont.port).to.equal    50001
        test(cont.nodeID).to.equal  '5bcd7d18db30ac06fa805c145eebd8f44b76baa4'

        test(cont.lastSeen).to.null

        done();