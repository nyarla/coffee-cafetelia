"use strict"

test  = require('chai').expect
utils = require('../src/utils')
msg   = require('../src/message')

describe 'Message:', ->

    it 'createMessage ok', ->
        from    =
            address: '127.0.0.1'
            port:    5000
            nodeID:  utils.id('127.0.0.1:5000')
        message = msg.createMessage 'ping', from,
            rpcID: utils.id('0123456789')

        test(message).to.be.an.instanceof msg.Message

        test(message.method).to.equal 'PING'

        test(message.from.nodeID).to.equal  utils.id('127.0.0.1:5000')
        test(message.from.address).to.equal '127.0.0.1'
        test(message.from.port).to.equal    5000

        test(message.rpcID).to.equal '87acec17cd9dcd20a716cc2cf67417b71c8a7016'
        test(message.replyTo).to.null

    it 'toRequest -> fromResponse ok', ->
        from =
            address: '127.0.0.1'
            port:    5000
            nodeID:  utils.id('127.0.0.1:5000')

        message = msg.createMessage('ping',from, rpcID: utils.id('0123456789') )
        request = message.toRequest()

        test(request.method).to.equal 'PING'

        test(request.nodeID).to.equal  from.nodeID
        test(request.address).to.equal from.address
        test(request.port).to.equal    from.port

        test(request.rpcID).to.equal   utils.id('0123456789')

        response = msg.fromResponse from, request

        test(response).to.be.an.instanceof msg.Message

        test(response.method).to.equal 'PING'

        test(response.from.nodeID).to.equal  from.nodeID
        test(response.from.address).to.equal from.address
        test(response.from.port).to.equal    from.port

        test(response.rpcID).to.equal utils.id('0123456789')
