"use strict"

test = require('chai').expect
utils = require('../src/utils')

describe 'Utils:', ->
    it 'exports.id ok', ->
        test( utils.id('127.0.0.1:5000') ).to.equal 'b660cd6180a4629b8e5f3c7eaeedcddf07dd1b1d'

    it 'exports.hex2Buffer ok', ->
        buf = utils.hex2Buffer utils.id('127.0.0.1:5000')

        test(buf.toString('hex')).to.equal 'b660cd6180a4629b8e5f3c7eaeedcddf07dd1b1d'

    it 'exports.bufferCompare ok', ->
        bufA = utils.hex2Buffer utils.id('127.0.0.1:5000')
        bufB = utils.hex2Buffer utils.id('127.0.0.1:5001')

        test( utils.bufferCompare( bufA, bufB ) ).to.equal 1
        test( utils.bufferCompare( bufB, bufA ) ).to.equal -1
        test( utils.bufferCompare( bufA, bufA ) ).to.equal 0

    it 'exports.idComapre ok', ->
        idA = utils.id('127.0.0.1:5000')
        idB = utils.id('127.0.0.1:5001')

        test( utils.idCompare( idA, idB ) ).to.equal 1
        test( utils.idCompare( idB, idA ) ).to.equal -1
        test( utils.idCompare( idA, idA ) ).to.equal 0

    it 'exports.distance ok', ->
        idA = utils.id('127.0.0.1:5000')
        idB = utils.id('127.0.0.1:5001')

        test( utils.distance( idA, idB ).toString('hex') ).to.equal '310d962717530b9fd969325b7af214c939130345'

    it 'exports.bucketIndex ok', ->
        idA = utils.id('127.0.0.1:5000')
        idB = utils.id('127.0.0.1:5001')

        test( utils.bucketIndex( idA, idB ) ).to.equal 157

    it 'exports.powerOfTwoBuffer ok', ->
        idx = 120

        test( utils.powerOfTwoBuffer(idx).toString('hex') ).to.equal '0000000001000000000000000000000000000000'

    it 'exports.randomInBucketRangeBuffer ok', ->
        idx = 120

        retA = utils.randomInBucketRangeBuffer idx
        retB = utils.randomInBucketRangeBuffer idx

        test( retA.toString('hex') ).to.match(/^([a-f0-9]{40})$/)
        test( retB.toString('hex') ).to.match(/^([a-f0-9]{40})$/)

        test( retA ).to.not.equal retB

        