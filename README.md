Cafetelia - Implementation of kademlia DHT written by CoffeeScript
==================================================================

1. What is this ?
-----------------

This package is a implementaion of kademlia DHT written by CoffeeScript.

This package was made with reference to [nikhilm's kademlia](https://github.com/nikhilm/kademlia).

2. TODO
-------

1. more tests, more documentations
2. more RPC and Storate implementation
3. network tests for build kademlia DHT network
4. ...more, ..more

3. Copyright and License
------------------------

Original Javascript code is written by Nikhil Marathe ([github:nikhilm](https://github.com/nikhilm)) and under MIT-license

CoffeeScript code is written by Naoki Okamura ([github:nyarla](https://github.com/nyarla)) and under MIT-license

