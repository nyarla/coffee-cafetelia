"use strict";

dgram = require 'dgram';

face = require './interface'

msg   = require '../message'
CONST = require '../constants'

class RPC extends face.RPC
    init: ( address, port, callback ) ->
        @socket = dgram.createSocket 'udp4', @onMessage.bind(@)
        @socket.bind port, address

    start: ->
        @expireRPCsTimerID = setInterval @expireRPCs.bind(@), CONST.TIMEOUT + 5

    stop: ->
        clearInterval @expireRPCsTimerID

    sendData: ( address, port, message ) ->
        data = new Buffer JSON.stringify( message.toRequest() ), 'utf8';
        @socket.send data, 0, data.length, port, address

    parseResponse: ( data, remote ) ->
        return msg.fromResponse remote, JSON.parse( data.toString('utf8') )
        
RPCServer = ( address, port, callback ) ->
    return new RPC address, port, callback

module.exports = RPCServer
