"use strict"

hat    = require 'hat'
assert = require 'assert'

msg    = require '../message'
CONST  = require '../constants'

class RPCTimeoutError extends Error
    constructor: ( rpcID, fn, line ) ->
        @rpcID = rpcID

        super "RPCID (#{rpcID}) is timeout.", fn, line

class RPC
    constructor: ( address, port, callback ) ->
        @callback      = callback || () ->
        @rack          = hat.rack CONST.B
        @awaitingReply = {}

        @init( address, port, callback )

    init: ( address, port, callback ) -> 

    start: ->
        throw new Error '`start` method is not implemented yet.'

    stop: ->
        throw new Error '`stop` method is not implemented yet.'

    send: ( to, message, cb ) ->
        assert.ok to,         'Node for target is not specified'
        assert.ok to.address, 'Node Address is not specified'
        assert.ok to.port,    'Node Port is not specified'

        assert.ok message instanceof msg.Message

        callback = cb || () ->

        message.rpcID = @rack()

        @sendData to.address, to.port, message
        
        if typeof(callback) == 'function'
            @awaitingReply[message.rpcID] =
                timestamp: Date.now()
                callback:  callback

    sendData: ( address, port, message ) ->
        throw new Error '`sendData` method is not implemented yet.'

    onMessage: ( data, remote ) ->
        message = null

        try
            message = @parseResponse data, remote
        catch e
            return

        if message.replyTo && @awaitingReply.hasOwnProperty( message.replyTo )
            
            rpcID = message.replyTo
            stock = @awaitingReply[rpcID]

            delete @awaitingReply[rpcID]

            stock.callback null, message
            return
            
        @callback null, message
        
    parseResponse: ( data, remote ) ->
        throw new Error '`parseResponse` is not implemented yet.'

    expireRPCs: ->
        now       = Date.now()
        discarded = 0

        Object.keys(@awaitingReply).forEach ( key ) =>
            stock = @awaitingReply[key]

            if stock && ( ( now - stock.timestamp ) > CONST.TIMEOUT )
                stock.callback new RPCTimeoutError(key), null
                delete @awaitingReply[key]

                discarded++

        return discarded

module.exports = exports =
    RPC: RPC
    RPCTimeoutError: RPCTimeoutError
