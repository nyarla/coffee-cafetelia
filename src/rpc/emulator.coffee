"use strict"

face   = require './interface'

utils  = require '../utils'
msg    = require '../message'
CONST  = require '../constants'

class Emulator
    constructor: ->
        @nodes  = {}

    register: ( address, port, callback ) ->
        nodeID = utils.id("#{address}:#{port}")

        @nodes[nodeID] = callback

    addQueue: ( address, port, message ) ->
        to =
            address: address
            port:    port
            nodeID:  utils.id("#{address}:#{port}")

        execute = =>
            @executeQueue to, message

        after = Math.floor((Math.random() * CONST.TIMEOUT) - CONST.TIMEOUT / 4)
            
        setTimeout execute, after
    
    executeQueue: ( sendTo, message ) ->
        callback = @nodes[ sendTo.nodeID ]

        if callback && @isSuccess( sendTo, message )
            from =
                address: message.address
                port:    message.port
            callback message, from

RPCEmulator = new Emulator()

class RPC extends face.RPC
    init: ( address, port, callback ) ->
        RPCEmulator.register address, port, @onMessage.bind(@)

    start: ->
        @expireRPCsTimerID = setInterval @expireRPCs.bind(@),  CONST.TIMEOUT + 5

    stop: ->
        clearInterval @expireRPCsTimerID

    sendData: ( address, port, message ) ->
        RPCEmulator.addQueue( address, port, message.toRequest() )

    parseResponse: ( data, remote ) ->
        message = msg.fromResponse( remote, data )
        return message

RPCServer = ( address, port, callback ) ->
    return new RPC address, port, callback

RPCServer.registerSuccessRule = ( isSuccess ) ->
    RPCEmulator.isSuccess = isSuccess

module.exports = RPCServer
