"use strict"

ok = require('assert').ok
B  = require('./constants').B

class Bucket
    constructor: ->
        @contacts = []

    get: ( idx ) ->
        ok idx >= 0
        ok idx < B

        return @contacts[idx]

    del: ( idx ) ->
        ok idx >= 0
        ok idx < B

        @contacts.splice idx, 1

        return @

    ins: ( idx, contact ) ->
        ok idx >= 0
        ok idx < B

        @contacts.splice idx, 0, contact

        return @

    exists: ( contact ) ->
        for node in @contacts
            if node.nodeID == contact.nodeID
                return true

        return false

    remove: ( contact ) ->
        idx = 0

        for node in @contacts
            if node.nodeID == contact.nodeID
                return @del idx

            idx++

        return @

    insert: ( contact ) ->
        idx  = 0
        last = @contacts.length
        seen = contact.lastSeen

        while idx < last
            mid = ( idx + last ) >>> 1

            if @contacts[mid] && ( @contacts[mid] || 0 ) < seen
                idx  = mid + 1
            else
                last = mid

        @contacts.splice idx, 0, contact

        return @

    size: ->
        return @contacts.length

module.exports = exports =
    Bucket:       Bucket
    createBucket: () -> new Bucket
