module.exports = exports =
    ALPHA:     3
    B:         160
    K:         20
    EXPIRE:    60 * 60 * 24 # 1 day
    REFRESH:   60 * 60     # 1 hour
    REPUBLISH: ( 60 * 60 * 24 ) + 10
    TIMEOUT:   1000
