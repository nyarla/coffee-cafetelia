"use strict"

assert  = require 'assert'
contact = require './contact'
utils   = require './utils'

class Message
    constructor: ( method, from, data = {} ) ->
        @method = method.toString().toUpperCase()

        @from   =
            nodeID:  from.nodeID
            address: from.address
            port:    from.port
        
        Object.freeze @from

        if data.rpcID
            @rpcID = data.rpcID
            delete data.rpcID
        else
            @rpcID = null

        if data.replyTo
            @replyTo = data.replyTo
            delete data.replyTo
        else
            @replyTo = null

        @data = data

    toRequest: ->
        data =
            method:  @method
            nodeID:  @from.nodeID
            address: @from.address
            port:    @from.port

        if @rpcID
            data.rpcID   = @rpcID

        if @replyTo
            data.replyTo = @replyTo 

        for key, val of @data
            data[key] = val

        Object.freeze data
    
        return data

Message.fromResponse = ( from, data ) ->
    assert.ok data.method, 'RPC method is missing'
    
    assert.ok data.nodeID,  'Node ID in response is missing'
    assert.ok data.address, 'Node Address in response is missing'
    assert.ok data.port,    'Node Port in response is missing'

    assert.equal(
        from.address
        data.address
        'Node Address in response does not match Source Address'
    )

    assert.equal(
        from.port
        data.port
        'Node Port in response does not match Source Port'
    )

    assert.equal(
        utils.id( [ data.address, data.port ].join(':') )
        data.nodeID
        'Node ID in response is mismatch'
    )

    method  = data.method
    address = data.address
    port    = data.port
    nodeID  = data.nodeID
    stash   = {}

    for key, val of data
        if key != 'method' || key != 'address' || key != 'port' || key != 'nodeID'
            stash[key] = val

    return new Message method,
        nodeID:  nodeID
        address: address
        port:    port
        , stash

module.exports = exports =
    Message: Message
    createMessage: ( type, from, data ) ->
        return new Message type, from, data
    fromResponse: ( from, data ) ->
        return Message.fromResponse from, data
