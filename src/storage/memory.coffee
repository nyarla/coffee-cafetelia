"use strict"

cache = require('node-cache')

class Cache
    constructor: ->
        @storage = new cache()

    set: ( key, val, cb ) ->
        @storage.set "#{key}", val, ( err, success ) ->
            cb err, success

    get: ( key, cb ) ->
        @storage.get "#{key}", ( err, data ) ->
            if err
                cb err, null
            else
                cb null, data[key]

module.exports = () ->
    return new Cache()
