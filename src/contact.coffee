"use strict"

crypto = require('crypto')
check  = require('validator').check

class Contact
    constructor: ( @address, @port ) ->
        check(@address, 'IP address is invalid').isIP()
        check(@port,    'Port number is invalid').min(0).max(65535).isInt()

        hash = crypto.createHash 'sha1'
        hash.update("#{address}:#{port}")

        @nodeID   = hash.digest 'hex'
        @lastSeen = null

    toString: ->
        return "#{@address}:#{@port} <#{@nodeID}>"

module.exports = exports =
    Contact:       Contact
    createContact: ( address, port ) ->
        return new Contact address, port
