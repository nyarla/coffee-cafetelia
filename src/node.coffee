"use strict"

assert = require 'assert'
async  = require 'async'

C = require './constants'
K =
    utils:   require './utils'
    bucket:  require './bucket'
    contact: require './contact'
    message: require './message'

class NoPeersError  extends Error
class NotFoundError extends Error

class KNode
    constructor: ( address, port, rpcFactory, storageFactory, debug ) ->
        # self node information
        @self = K.contact.createContact address, port
        Object.freeze @self

        @storage = storageFactory()
        @buckets = {}
        @rpc     = rpcFactory( address, port, @dispatch.bind(@) )

        @debug   = debug || false

    MSG: ( method, data ) ->
        return K.message.createMessage method, @self, data

    LOG: ( label, message, args... ) ->
        return if not @debug
        
        console.log "<%s:%s> [#{label}] #{message}",
            @self.address, @self.port, args...

    WARN: ( label, message, args... ) ->
        console.warn "<%s:%s> [#{label}] #{message}",
            @self.address, @self.port, args...

    dispatch: ( err, message ) ->
        if err
            @LOG 'DISPATCH', 'Error in respond: %s', err
            return

        from       = message.from
        contact    = K.contact.createContact from.address, from.port

        actionName = "action_#{message.method.toLowerCase()}"
        action     = @[actionName]

        if action && typeof(action) == 'function'
            @LOG 'DISPATCH', 'prepare'
            @update_contact contact
            @LOG 'DISPATCH', 'action: ', message.method
            action.bind(@)(message)
        else
            @WARN 'RPC', 'Unknown RPC method: `%s`', message.method

    update_contact: ( contact, callback ) ->
        if not contact
            return
    
        callback ?= () ->

        bucketIdx = K.utils.bucketIndex @self.nodeID, contact.nodeID
        assert.ok bucketIdx < C.B

        if not @buckets.hasOwnProperty bucketIdx
            @buckets[bucketIdx] = K.bucket.createBucket()

        bucket = @buckets[bucketIdx]
        contact.lastSeen = Date.now()

        @LOG 'UPDATE', 'update contact: %s', contact
    
        if bucket.exists(contact)
            bucket.remove contact
            bucket.insert contact
            callback()
            @LOG 'UPDATE', 'done reinsert: %s', contact
        else if bucket.size() < C.K
            bucket.insert contact
            callback()
            @LOG 'UPDATE', 'done insert: %s', contact
        else
            @rpc.send bucket.get(0), @MSG('PING'), ( err, message ) =>
                if err
                    bucket.del 0
                    bucket.insert contact

                callback()
                @LOG 'UPDATE', 'done update: %s', contact

    find_closest_nodes: ( key, howMany, exclude ) ->
        contacts = []

        addContact = ( contact ) ->
            if not contact
                return
            if contacts.length >= howMany
                return
            if contact.nodeID == exclude
                return

            contacts.push contact

        addClosestFromBucket = ( bucket ) ->
            distances = []

            bucket.contacts.forEach ( val ) ->
                distances.push
                    distance: K.utils.distance val.nodeID, key
                    contact:  val

            distances.sort ( a, b ) ->
                return K.utils.bufferCompare( a.distance, b.distance )

            distances.splice( 0, howMany - contacts.length ).forEach ( val ) ->
                addContact val.contact

        bucketIdx = K.utils.bucketIndex @self.nodeID, key
        if @buckets.hasOwnProperty bucketIdx
            addClosestFromBucket @buckets[ bucketIdx ]

        oldBucketIdx = bucketIdx
        while contacts.length < howMany && bucketIdx < C.B
            bucketIdx++
            if @buckets.hasOwnProperty bucketIdx
                addClosestFromBucket @buckets[ bucketIdx ]

        bucketIdx = oldBucketIdx
        while contacts.length < howMany && bucketIdx >= 0
            bucketIdx--
            if @buckets.hasOwnProperty bucketIdx
                addClosestFromBucket @buckets[ bucketIdx ]

        return contacts

    refresh_bucket: ( idx, cb ) ->
        random = K.utils.randomInBucketRangeBuffer idx
        @iterative_find_node random.toString('hex'), cb

    iterative_find: ( key, mode, cb ) ->
        assert.ok mode == 'NODE' || mode == 'VALUE'

        externalCallback     = cb || () ->
        closestNode          = null;
        previousClosestNode  = null;
        shortList            = @find_closest_nodes key, C.ALPHA, @self.nodeID
        contacted            = {}
        foundValue           = false
        value                = null
        contactsWithoutValue = []

        closestNode = shortList[0]

        if not closestNode
            externalCallback(
                new NoPeersError('Not connected to overlay network. No Peers'),
                mode,
                null
            )
            return

        closestNodeDistance = K.utils.distance key, closestNode.nodeID

        reject = ( list, cb ) ->
            newList = []
            for item in list
                if not cb(item)
                    newList.push item
            return newList

        unique = ( list ) ->
            newList = []
            added   = {}

            for item in list
                if not added[ item.nodeID ]
                    newList.push item
                    added[ item.nodeID ] == true

            return newList

        bufferCompare = ( a, b ) ->
            return K.utils.bufferCompare a.distance, b.distance
        
        asyncIter = ( contact, callback ) =>
            @rpc.send contact,
                @MSG("FIND_#{mode}", key: key),
                ( err, message ) =>
                    if err
                        @LOG "INTERNAL", "Error in iterative find: %s: %s: %s",
                            mode, contact, err
                        shortList = reject shortList, ( val ) ->
                            return val.nodeID == contact.nodeID
                    else
                        @LOG 'INTERNAL', "update contact"
                        @update_contact contact
                        contacted[contact.nodeID] = true

                        dist = K.utils.distance key, contact.nodeID
                        
                        if K.utils.bufferCompare(dist, closestNodeDistance)==-1
                            previousClosestNode = closestNode
                            closestNode         = contact
                            closestNodeDistance = dist

                        data = message.data

                        if data.status && data.status=='found' && mode=='VALUE'
                            foundValue = true
                            value      = data.value
                        else
                            if mode == 'VALUE'
                                contactsWithoutValue.push contact

                            data.value.forEach ( val ) ->
                                shortList.push K.contact.createContact(
                                    val.address, val.port
                                )

                            shortList = unique shortList
                    callback()
                    @LOG 'INTERNAL', 'Request processed.'

        asyncCb = ( results ) =>       
            if foundValue
                selfNodeID = @self.nodeID

                distances  = []
                contactsWithoutValue.forEach ( val ) ->
                    distances.push
                        distance: K.utils.distance( val.nodeID, selfNodeID )
                        contact:  contact

                distances.sort bufferCompare

                if distances.length >= 1
                    closestWithoutValue = distances[0].contact
                    @LOG 'INTERNAL', 'Closest is: %s', closestWithoutValue
                    @rpc.send closestWithoutValue,
                        @MSG('STORE', key: key, value: value)

                externalCallback null, 'VALUE', value
                @LOG 'INTERNAL', 'Value of key (%s) is found: %s', key, value
                return

            if closestNode == previousClosestNode || shortList.length > C.K
                externalCallback null, 'NODE', shortList
                @LOG 'INTERNAL', 'Max contacted'
                return

            remain = reject shortList, ( val ) -> return contacted[val.nodeID]
            
            if remain.length == 0
                externalCallback null, 'NODE', shortList
                @LOG 'INTERNAL', 'Contacted all'
            else
                xyz.bind(@)(remain.splice( 0, C.ALPHA ))

        xyz = ( alphaContacts ) =>
            async.filter alphaContacts, asyncIter, asyncCb
            

        @LOG 'INTERNAL', 'iterative find: mode => %s', mode
        xyz.bind(@)(shortList)

    iterative_find_node: ( nodeID, cb ) ->
        @LOG 'INTERNAL', 'find node <%s>', nodeID
        @iterative_find nodeID, 'NODE', cb

    iterative_find_value: ( key, cb ) ->
        cb ?= () ->

        @iterative_find key, 'VALUE', ( err, type, val ) ->
            if type == 'VALUE'
                cb null, val
            else
                cb new NotFoundError("Value of `#{key}` is not found."), null

    action_ping: ( message ) ->
        @rpc.send message.from, @MSG('PONG', replyTo: message.rpcID)

    action_pong: ( message ) ->

    action_store: ( message ) ->
        data = message.data

        if not data
            return

        if ! data.key || data.key.length != C.B / 4
            return

        if ! data.value
            return

        @storage.set data.key, data.value, ( err, success ) =>
            @rpc.send message.from,
                @MSG('STORE_REPLY', replyTo: message.rpcID, status: !! success)

    action_store_reply: ( message ) ->

    action_find_value: ( message ) ->
        data = message.data

        if not data
            return

        if ! data.key || data.key.length != C.B / 4
            return

        from    = message.from
        contact = K.contact.createContact from.address, from.port

        @storage.get data.key, ( err, val ) =>
            if err || not val
                contacts = @find_closest_nodes data.key, C.K, contact.nodeID

                @rpc.send contact,
                    @MSG('FIND_VALUE_REPLY',
                        replyTo: message.rpcID
                        status:  'notfound',
                        value:   contacts
                    )
            else
                @rpc.send contact,
                    @MSG('FIND_VALUE_REPLY',
                        replyTo: message.rpcID
                        status:  'found'
                        value:   val
                    )

    action_find_value_reply: ( message ) ->
        

    action_find_node: ( message ) ->
        data = message.data

        if not data
            return

        if ! data.key || data.key.length != C.B / 4
            return

        contacts = @find_closest_nodes data.key, C.K, message.from

        from = message.from
        contact = K.contact.createContact from.address, from.port

        @rpc.send contact,
            @MSG('FIND_NODE_REPLY', replyTo: message.rpcID, value: contacts)

    action_find_node_reply: ( message ) ->
        

    toString: () ->
        return "KNode #{@self.address}:#{@self.port} <#{@self.nodeID}>"

    start: () ->
        @rpc.start()

    stop: () ->
        @rpc.stop()

    connect: ( address, port, cb ) ->
        cb     ?= () ->
        assert.ok @self.nodeID
        contact = K.contact.createContact address, port

        refresh = ( type, contacts, asyncCallback ) =>
            leastBucket = null
            Object.keys(@buckets).forEach ( val ) ->
                if not leastBucket
                    leastBucket = val
                else
                    if val < leastBucket
                        leastBucket = val

            bucketToRefresh = []
            Object.keys(@buckets).forEach ( val ) ->
                if val >= leastBucket
                    bucketToRefresh.push val

            queue = async.queue @refresh_bucket.bind(@), 1

            bucketToRefresh.forEach ( bucketIdx ) ->
                queue.push bucketIdx

            asyncCallback()

        async.waterfall([
            @update_contact.bind(@, contact),
            @iterative_find_node.bind(@, @self.nodeID),
            refresh.bind(@),
        ], cb)

    get: ( key, cb ) ->
        cb ?= () ->
        @iterative_find_value K.utils.id(key), cb

    set: ( key, val, cb ) ->
        cb   ?= () ->
        keyID = K.utils.id(key)
        msg   = @MSG('STORE', key: keyID, value: val)

        @iterative_find_node keyID, ( err, type, contacts ) =>
            if err
                cb err
                return

            async.forEach(
                contacts,
                ( contact, asyncCallback ) =>
                    @rpc.send contact, msg, ( err, message ) ->
                        asyncCallback(null)
                , cb
             )

module.exports = ( args... ) ->
    return new KNode args...

module.exports.KNode         = KNode
module.exports.NoPeersError  = NoPeersError
module.exports.NotFoundError = NotFoundError