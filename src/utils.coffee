"use strict"

assert = require('assert')
crypto = require('crypto')

CONST  = require('./constants')

exports.id = ( str ) ->
    hash = crypto.createHash 'sha1'
    hash.update str
    return hash.digest 'hex'

exports.hex2Buffer = ( str ) ->
    ret = new Buffer CONST.K
    ret.write str, 0, 'hex'
    return ret

exports.bufferCompare = ( a, b ) ->
    assert.equal a.length, b.length

    for i in [ 0 .. a.length - 1 ]
        if a[i] != b[i]
            if a[i] < b[i]
                return -1
            else
                return 1

    return 0

exports.idCompare = ( a, b ) ->
    aBuf = exports.hex2Buffer a
    bBuf = exports.hex2Buffer b

    return exports.bufferCompare aBuf, bBuf

exports.distance = ( a, b ) ->
    assert.ok(a)
    assert.ok(b)
    
    ret  = new Buffer CONST.K
    aBuf = exports.hex2Buffer a
    bBuf = exports.hex2Buffer b

    for i in [ 0 .. CONST.K - 1 ]
        ret[i] = aBuf[i] ^ bBuf[i]

    return ret

exports.bucketIndex = ( a, b ) ->
    distance = exports.distance a, b
    idx      = CONST.B

    for i in [ 0 .. distance.length - 1 ]
        if distance[i] == 0
            idx -= 8
            continue

        for j in [ 0 .. 7 ]
            if distance[i] & ( 0x80 >> j )
                return --idx
            else
                idx--;

    return idx

exports.powerOfTwoBuffer = (exp) ->
    assert.ok exp >= 0 && exp < CONST.B
    buf = new Buffer CONST.K
    buf.fill 0

    byte = parseInt exp / 8
    buf[CONST.K - byte - 1] = 1 << ( exp % 8 )

    return buf

exports.randomInBucketRangeBuffer = ( bucketIdx ) ->
    base = exports.powerOfTwoBuffer bucketIdx
    byte = parseInt bucketIdx / 8

    i = CONST.K - 1
    while i > ( CONST.K - byte - 1 )
        i--
        base[i] = parseInt Math.random() * 256

    i = bucketIdx - 1
    while i >= byte * 8
        i--

        one = Math.random() >= 0.5;
        shiftAmount = i - byte * 8;
        base[CONST.K - byte - 1] |= one ? 1 << shiftAmount : 0 ;

    return base
