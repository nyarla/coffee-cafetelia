#!/usr/bin/env coffee

util   = require('../src/utils')
knode  = require('../src/node')
rpc    = require('../src/rpc/emulator')
store  = require('../src/storage/memory')

host   = '127.0.0.1'
port   = 5000

self   = knode( host, port, rpc, store, false)
others = []

self.start()

rpc.registerSuccessRule () ->   return true


for idx in [ 5100 .. 5120 ]
    otherNode = knode( host, idx, rpc, store )
    otherNode.start()

    others.forEach ( node ) ->
        otherNode.connect node.self.address, node.self.port

    others.push otherNode
    self.connect otherNode.self.address, otherNode.self.port

command = ( stream ) ->
    stream.setEncoding('utf8')
    stream.on 'data', ( data ) ->
        parts = data.trim().split(' ')

        switch parts[0]
            when "set"
                if parts.length < 3
                    stream.write("Not enough parameters\n")
                    break

                self.set parts[1], parts[2], ( err ) ->
                    if err
                        stream.write("Error settings #{parts[1]}: #{JSON.stringify(err)} \n")
                    else
                        stream.write("Set #{parts[1]} => #{parts[2]}\n")
            when "get"
                if parts.length < 2
                    stream.write "Not enough parameters\n"
                    break

                self.get parts[1], ( err, val ) ->
                    if err
                        stream.write("Error getting #{parts[1]}: #{JSON.stringify(err)}\n")
                    else
                        stream.write("Get #{parts[1]} -> #{val}\n")
            else
                stream.write("Unknown command\n")

process.stdin.resume()
command(process.stdin)
